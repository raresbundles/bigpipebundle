<?php

namespace Rares\BigPipeBundle\Twig;

use Rares\BigPipeBundle\Service\BigPipeService;

class BigPipeExtension extends \Twig_Extension
{
    private $service;
    private $pageletId;

    public function __construct(BigPipeService $service)
    {
        $this->service = $service;
        $this->pageletId = 0;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('render_bigpipe', [$this, 'renderBigPipe'], [
                'is_safe' => ['html'],
            ]),
        ];
    }

    public function renderBigPipe($uri, $priority = 0)
    {
        if (!is_int($priority)) {
            throw new \Exception("Big Pipe Pagelet priority must be an integer");
        }

        $this->service->addPagelet([
            'uri' => $uri,
            'id' => $this->pageletId,
            'priority' => $priority,
        ]);

        return '<span id="big-pipe-pagelet-' . $this->pageletId++ . '"></span>';
    }
}