RaresBigPipe = function () {
    this.pagelets = new Array();
};

/**
 * @param data = {
 *      "id":   "big-pipe-pagelet-{id}",
 *      "html": "html content",
 *  }
 */
RaresBigPipe.prototype.onPageletArrive = function(data) {
    this.pagelets.push(data);
    this._showHTML(data.id, data.html);
}

RaresBigPipe.prototype._showHTML = function(id, html) {
    $('#' + id).replaceWith(html);
}
