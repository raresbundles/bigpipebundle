<?php

namespace Rares\BigPipeBundle\Cache;

use Symfony\Bundle\FrameworkBundle\HttpCache\HttpCache;
use Symfony\Component\HttpKernel\HttpCache\StoreInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class BigPipeHttpCache extends HttpCache
{
    /**
     * Since the kernel can not be a service, save the current instance here
     * if it exists so the big pipe service knows to use cache or not.
     *
     * @var null|BigPipeHttpCache
     */
    public static $instance = null;

    private $store;

    /**
     * A decorator class used for decorating the custom big pipe kernel
     * to be used instead of the default symfony http cache kernel when
     * wanting to use cache.
     *
     * @param KernelInterface $kernel
     * @param StoreInterface|null $store
     */
    public function __construct(KernelInterface $kernel, StoreInterface $store = null)
    {
        $this->store = $store;

        parent::__construct(new BigPipeHttpKernel($kernel), null);

        self::$instance = $this;
    }

    protected function createStore()
    {
        return $this->store ?: parent::createStore();
    }
}