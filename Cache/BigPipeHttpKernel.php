<?php


namespace Rares\BigPipeBundle\Cache;

use Rares\BigPipeBundle\Service\BigPipeService;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\HttpKernel\TerminableInterface;

class BigPipeHttpKernel implements KernelInterface, TerminableInterface
{
    /** @var KernelInterface $kernel */
    private $kernel;

    /**
     * Decorator class for the default kernel.
     *
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * The Symfony http cache handles all cache requests as master requests which
     * will break compatibility with the way big pipe is handled, so if a request
     * is a big pipe (pagelet) request, then it will be hanlded as a sub-request.
     *
     * @param Request $request
     * @param int $type
     * @param bool $catch
     * @return Response
     */
    public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = true)
    {
        if ($request->attributes->has(BigPipeService::BIG_PIPE_SUB_REQUEST)) {
            return $this->kernel->handle($request, self::SUB_REQUEST, $catch);
        }

        return $this->kernel->handle($request, $type, $catch);
    }

    public function terminate(Request $request, Response $response)
    {
        $this->kernel->terminate($request, $response);
    }

    public function registerBundles()
    {
        return $this->kernel->registerBundles();
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $this->kernel->registerContainerConfiguration($loader);
    }

    public function boot()
    {
        $this->kernel->boot();
    }

    public function shutdown()
    {
        $this->kernel->shutdown();
    }

    public function getBundles()
    {
        return $this->kernel->getBundles();
    }

    public function getBundle($name)
    {
        return $this->kernel->getBundle($name);
    }

    public function locateResource($name, $dir = null, $first = true)
    {
        return $this->kernel->locateResource($name, $dir, $first);
    }

    public function getName()
    {
        return $this->kernel->getName();
    }

    public function getEnvironment()
    {
        return $this->kernel->getEnvironment();
    }

    public function isDebug()
    {
        return $this->kernel->isDebug();
    }

    public function getRootDir()
    {
        return $this->kernel->getRootDir();
    }

    public function getContainer()
    {
        return $this->kernel->getContainer();
    }

    public function getStartTime()
    {
        return $this->kernel->getStartTime();
    }

    public function getCacheDir()
    {
        return $this->kernel->getCacheDir();
    }

    public function getLogDir()
    {
        return $this->kernel->getLogDir();
    }

    public function getCharset()
    {
        return $this->kernel->getCharset();
    }

    public function serialize()
    {
        return $this->kernel->serialize();
    }

    public function unserialize($serialized)
    {
        $this->kernel->unserialize($serialized);
    }
}