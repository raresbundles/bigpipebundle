<?php

namespace Rares\BigPipeBundle\EventSubscriber;

use Psr\Container\ContainerInterface;
use Rares\BigPipeBundle\Service\BigPipeService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpKernel\EventListener\AbstractSessionListener;
use Symfony\Component\HttpKernel\KernelEvents;

class BigPipeEventSubscriber extends AbstractSessionListener
{
    private $service;

    public function __construct(BigPipeService $service, ContainerInterface $container)
    {
        $this->service = $service;
        $this->container = $container;
    }

    protected function getSession()
    {
        if (!$this->container->has('session')) {
            return;
        }

        return $this->container->get('session');
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => ['onKernelRequest', 128],
            // low priority to come after regular response listeners, but higher than StreamedResponseListener
            KernelEvents::RESPONSE => ['onKernelResponse', -1000],
            KernelEvents::TERMINATE => 'onKernelTerminate',
        ];
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        if (!$session = $this->container && $this->container->has('initialized_session') ? $this->container->get('initialized_session') : $event->getRequest()->getSession()) {
            return;
        }

        $response = $event->getResponse();

        if ($session->isStarted() || ($session instanceof Session && $session->hasBeenStarted())) {
            if (!$response->headers->has(self::NO_AUTO_CACHE_CONTROL_HEADER)) {
                $response
                    ->setPrivate()
                    ->setMaxAge(0)
                    ->headers->addCacheControlDirective('must-revalidate');
            }
        }

        // Always remove the internal header if present
        $response->headers->remove(self::NO_AUTO_CACHE_CONTROL_HEADER);

        // Do not save session for big pipe requests, only on kernel terminate
        if (!$event->getRequest()->attributes->has(BigPipeService::BIG_PIPE_REQUEST)
            && $session->isStarted()
        ) {
            $session->save();
        }
    }

    /**
     * Save the session when big pipe request ends
     *
     * @param PostResponseEvent $event
     */
    public function onKernelTerminate(PostResponseEvent $event)
    {
        if (!$event->isMasterRequest() || !$event->getRequest()->attributes->has(BigPipeService::BIG_PIPE_REQUEST)) {
            return;
        }

        if (!$session = $this->container && $this->container->has('initialized_session') ? $this->container->get('initialized_session') : $event->getRequest()->getSession()) {
            return;
        }

        if ($session->isStarted()) {
            $session->save();
        }
    }
}