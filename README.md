# Overview
The BigPipeBundle is a bundle for Symfony 3.4/4 which integrates a basic BigPipe concept into Symfony applications easily.More information about the concept can be found [here](https://www.facebook.com/notes/facebook-engineering/bigpipe-pipelining-web-pages-for-high-performance/389414033919/).

# Installation
To install this bundle, first you need to have jQuery installed.

Then you need to require this bundle:

```
    composer require "rares/big-pipe-bundle"
```

Then enable the bundle in your AppKernel file:

```php
$bundles = [
	...,
	new Rares\BigPipeBundle\RaresBigPipeBundle(),
];
```


You will then have to install the assets using the command:

```
bin/console assets:install --symlink
```

Then you need to include the scripts from this bundle into your template:

```twig
<script src="{{ asset('bundles/raresbigpipe/js/scripts.js') }}"></script>
```

# Usage
To use this bundle, simply call the Twig function **render_bigpipe** inside your templates where you would normally use other Twig render functions.This function will render a placeholder tag for a pagelet and then after the main page is loaded, all the pagelets on a page will be loaded in order.The function supports paths and controller references, just like the normal render function.

The function also takes an optional second argument which is the priority of the pagelet.Pagelets with higher priority will be rendered first on a page. 

Example:

```twig
    {{ render_bigpipe('url...') }}<br>
    {{ render_bigpipe(path('route...')) }}<br>
    {{ render_bigpipe(controller('Controller::...'), 3) }}
```

Then, in your controller you will have to use the **renderBigPipe** method from the **BigPipeRenderer** class:

```php
	return $this->get(BigPipeRenderer::class)->renderBigPipe('page.html.twig', [
		...
	]);
```

That's it!You now have a working application using this bundle!Your main page will render first, followed by all the pagelets in the order they were defined on the page.

# Caching
This bundle also has integration with the Symfony Reverse Proxy for caching.In fact, it is recommended to enable caching in your application.To enable caching, simplysimple edit your web/app.php file and instead of using the default Symfony AppCache kernel, use the BigPipeHttpCache class provided by this bundle:

```php
$kernel = new \Rares\BigPipeBundle\Cache\BigPipeHttpCache($kernel);
```

You can also pass a custom StoreInterface as the second argument to the BigPipeHttpCache class.This is useful if you want to use this bundle together with the [ReverseProxyBundle](https://bitbucket.org/raresbundles/reverseproxybundle).

# Session

By default, the session in Symfony is saved on the kernel response event, which works fine for a normal application but is not compatible with the big pipe bundle
since the response is stream and headers are sent first which would prevent accessing the session in pagelets.Because of this, this bundle overrides this behaviour and instead
saves the session after the kernel terminate event.If you have problems with the session, you can save it manually but keep in mind that this will prevent it from being
used by pagelets that will be loaded after that particular sub-request.

**Take a look at the [demo](https://bitbucket.org/raresbundles/bigpipebundle/branch/demo) branch to see an example project that uses this bundle.**
