<?php

namespace Rares\BigPipeBundle\Renderer;

use Rares\BigPipeBundle\Service\BigPipeService;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Templating\StreamingEngineInterface;

class BigPipeRenderer
{
    private $service;
    private $engine;
    private $stack;

    public function __construct(BigPipeService $service, StreamingEngineInterface $engine,
        RequestStack $stack)
    {
        $this->service = $service;
        $this->engine = $engine;
        $this->stack = $stack;
    }

    public function renderBigPipe($template, $args = [])
    {
        $request = $this->stack->getMasterRequest();
        $request->attributes->set(BigPipeService::BIG_PIPE_REQUEST, true);

        return StreamedResponse::create(function() use ($template, $args, $request) {
            $this->engine->stream($template, $args);
            $this->service->renderPagelets($request);
        });
    }
}