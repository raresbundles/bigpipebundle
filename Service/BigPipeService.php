<?php

namespace Rares\BigPipeBundle\Service;

use Rares\BigPipeBundle\Cache\BigPipeHttpCache;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ControllerReference;
use Symfony\Component\HttpKernel\Fragment\RoutableFragmentRenderer;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class BigPipeService extends RoutableFragmentRenderer
{
    const BIG_PIPE_REQUEST = 'rares_big_pipe_request';
    const BIG_PIPE_SUB_REQUEST = 'rares_big_pipe_sub_request';

    private $stack;
    private $kernel;
    private $cache;
    private $storage;

    private $pagelets;

    public function __construct(RequestStack $stack, HttpKernelInterface $kernel, TokenStorageInterface $storage)
    {
        $this->stack = $stack;
        $this->kernel = $kernel;
        $this->cache = BigPipeHttpCache::$instance;
        $this->pagelets = [];
        $this->storage = $storage;
    }

    public function addPagelet($pagelet)
    {
        $this->pagelets[] = $pagelet;
    }

    /**
     * Render all the pagelets on a page, in order of their priority.
     *
     * @param Request $request
     */
    public function renderPagelets(Request $request)
    {
        if (!empty($this->pagelets)) {
            flush();
            echo "<script>var bigPipe = new RaresBigPipe();</script>";

            $this->stack->push($request);
            usort($this->pagelets, function($v1, $v2) {
                if ($v1['priority'] > $v2['priority']) {
                    return -1;
                } else {
                    return 1;
                }
            });

            foreach ($this->pagelets as $pagelet) {
                $response = $this->render($pagelet['uri'], $request);

                $response->headers->remove('X-Debug-Token');

                $pagelet = [
                    'id' => 'big-pipe-pagelet-' . $pagelet['id'],
                    'html' => $response->getContent(),
                    // TODO: Add ability to load css and javascript files async from a pagelet
                    'css' => '',
                    'js' => '',
                ];

                echo '<script>bigPipe.onPageletArrive(' . json_encode($pagelet) . ');</script>';
                flush();
            }
            $this->stack->pop($request);
        }
    }

    /**
     * Render a pagelet from a url or controller reference.Depending if caching is used
     * or not, different kernels will be used to actually render the sub-request.
     *
     * @param string|ControllerReference $uri
     * @param Request $request
     * @param array $options
     * @return Response
     */
    public function render($uri, Request $request, array $options = [])
    {
        $reference = null;
        if ($uri instanceof ControllerReference) {
            $reference = $uri;

            // Remove attributes from the generated URI because if not, the Symfony
            // routing system will use them to populate the Request attributes. We don't
            // want that as we want to preserve objects (so we manually set Request attributes
            // below instead)
            $attributes = $reference->attributes;
            $reference->attributes = [];

            // The request format and locale might have been overridden by the user
            foreach (['_format', '_locale'] as $key) {
                if (isset($attributes[$key])) {
                    $reference->attributes[$key] = $attributes[$key];
                }
            }

            $uri = $this->generateFragmentUri($uri, $request, false, false);

            $reference->attributes = array_merge($attributes, $reference->attributes);
        }

        $subRequest = Request::create($uri, Request::METHOD_GET, [], $request->cookies->all(), [], $request->server->all());
        $subRequest->attributes->set(self::BIG_PIPE_SUB_REQUEST, true);

        if ($this->cache) {
            $response = $this->cache->handle($subRequest, HttpKernelInterface::SUB_REQUEST, false);
        } else {
            $response = $this->kernel->handle($subRequest, HttpKernelInterface::SUB_REQUEST, false);
        }

        return $response;
    }

    public function getName()
    {
        return 'big_pipe';
    }
}